#!/bin/sh
#
# gwck bmx6 module
#
# Copyright (C) 2008-2012 Simó Albert i Beltran
#
# Contributors:
#	Pau Escrich <p4u@dabax.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Version 0.1.20111106.2


#############
# Variables #
#############

# bmx6 tunInNet value.
[ -z "$GWCK_MOD_BMX6_TUN_IN_NET" ] \
&& GWCK_MOD_BMX6_TUN_IN_NET="0.0.0.0/0"

# bmx6 tunInNet bandwith value.
[ -z "$GWCK_MOD_BMX6_BANDWIDTH" ] \
&& GWCK_MOD_BMX6_BANDWIDTH="1000000"

# bmx6 tunOut value.
[ -z "$GWCK_MOD_BMX6_TUN_OUT" ] \
&& GWCK_MOD_BMX6_TUN_OUT="ipv4Default"

# bmx6 tunOut network value.
[ -z "$GWCK_MOD_BMX6_NETWORK" ] \
&& GWCK_MOD_BMX6_NETWORK="0.0.0.0/0"

# Not disable offer if no other nodes offer Internet access.
[ -z "$GWCK_MOD_BMX6_TEST_OTHER_OFFER" ] \
&& GWCK_MOD_BMX6_TEST_OTHER_OFFER="true"


# Disable offer command.
[ -z "$GWCK_MOD_BMX6_DISABLE_OFFER" ] \
&& GWCK_MOD_BMX6_DISABLE_OFFER="bmx6 -c tunInNet=-$GWCK_MOD_BMX6_TUN_IN_NET"

# Enable offer command.
[ -z "$GWCK_MOD_BMX6_ENABLE_OFFER" ] \
&& GWCK_MOD_BMX6_ENABLE_OFFER="bmx6 -c tunInNet=$GWCK_MOD_BMX6_TUN_IN_NET /bandwidth=$GWCK_MOD_BMX6_BANDWIDTH"

# Disable search command.
[ -z "$GWCK_MOD_BMX6_DISABLE_SEARCH" ] \
&& GWCK_MOD_BMX6_DISABLE_SEARCH="bmx6 -c tunOut=-$GWCK_MOD_BMX6_TUN_OUT"

# Enable search command.
[ -z "$GWCK_MOD_BMX6_ENABLE_SEARCH" ] \
&& GWCK_MOD_BMX6_ENABLE_SEARCH="bmx6 -c tunOut=$GWCK_MOD_BMX6_TUN_OUT /network=$GWCK_MOD_BMX6_NETWORK"


#############
# Functions #
#############

##
# Determine if this node offer Internet access.
#
gwck_mod_bmx6_this_node_offer()
{
	local offer="$(bmx6 -c --parameters | while read key val
	do
		if [ "$key" = "tunInNet" ] && [ "$val" = "$GWCK_MOD_BMX6_TUN_IN_NET" ]
			then
			read key val
			if [ "$key" = "/bandwidth" ] && [ "$val" = "$GWCK_MOD_BMX6_BANDWIDTH" ]
			then
				echo "true"
			fi
		fi
	done)"

	[ -z "$offer" ] && offer="false"
	if $offer
	then
		log "Detect this node offer internet access."
		return 0
	else
		return 1
	fi
}

##
# Determine if this node search Internet access.
#
gwck_mod_bmx6_this_node_search()
{
	local search="$(bmx6 -c --parameters | while read key val
	do
		if [ "$key" = "tunOut" ] && [ "$val" = "$GWCK_MOD_BMX6_TUN_OUT" ]
		then
			read key val
			if [ "$key" = "/network" ] && [ "$val" = "$GWCK_MOD_BMX6_NETWORK" ]
			then
				echo "true"
			fi
		fi
	done)"
	[ -z "$search" ] && search="false"
	if $search
	then
		log "Detect this node search internet access."
		return 0
	else
		return 1
	fi
}

##
# Determine if other nodes are offered any internet access.
#
gwck_mod_bmx6_are_inet_offered_by_other_node()
{
	# TODO
	return 0
}

##
# Change to offering Internet access.
#
gwck_mod_bmx6_change_to_offer()
{
	if gwck_mod_bmx6_this_node_search
	then
		$GWCK_MOD_BMX6_DISABLE_SEARCH 2>&1 | log \
		&& log "Disable searching Internet access with BMX6."
	fi
	if ! gwck_mod_bmx6_this_node_offer
	then
		$GWCK_MOD_BMX6_ENABLE_OFFER 2>&1 | log \
		&& log "Offering Internet access with BMX6."
	fi
}

##
# Change to searching Internet access.
#
gwck_mod_bmx6_change_to_search()
{
	if gwck_mod_bmx6_this_node_offer
	then
		$GWCK_MOD_BMX6_DISABLE_OFFER 2>&1 | log \
		&& log "Disable offering Internet access with BMX6."
	fi
	if ! gwck_mod_bmx6_this_node_search
	then
		$GWCK_MOD_BMX6_ENABLE_SEARCH 2>&1 | log \
		&& log "Searching Internet access with BMX6."
	fi
}


##############
# gwck hooks #
##############

gwck_mod_bmx6_init()
{
	gwck_mod_bmx6_change_to_offer
}

gwck_mod_bmx6_connect()
{
	# Enable offer direct Internet access with BMX6.
	gwck_mod_bmx6_change_to_offer
}

gwck_mod_bmx6_disconnect()
{
	# Are other node offering any internet access?
	if gwck_mod_bmx6_are_inet_offered_by_other_node || ! "$GWCK_MOD_BMX6_TEST_OTHER_OFFER"
	then
		# Enabled searching Internet access with BMX6.
		gwck_mod_bmx6_change_to_search
	else
		# Print no change state message if don't are other modes offering internet access.
		"$LOOP" && log "No other node offering internet access with BMX6."
		# Save fake state of connection.
		connect = "true"
	fi
}

#gwck_mod_bmx6_term()
#{
#	:
#}

#############
# Main code #
#############

# Actions in run-time load module.
:
